package lab4;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

enum UZnak {
	IDN, BROJ, OP_PRIDRUZI,	OP_PLUS, OP_MINUS, OP_PUTA, OP_DIJELI, L_ZAGRADA, D_ZAGRADA, KR_ZA, KR_OD, KR_DO, KR_AZ;
}

class Jedinka {
	UZnak znak;
	int red;
	String nizZnakova;
	int definiraniBlok;
	
	public Jedinka(UZnak znak, int red, String nizZnakova, int definiraniBlok) {
		this.znak = znak;
		this.red = red;
		this.nizZnakova = nizZnakova;
		this.definiraniBlok = definiraniBlok;
	}
	
	public String toString() {
		return znak + " " + red + " " + nizZnakova + " " + definiraniBlok;
	}
}

public class FRISCGenerator{
	
	//blok je dubina
	static int blok = 0;

	static String ispis = "\tMOVE 40000, R7\n\n";
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		String[] arraySplit;
		ArrayList<String> varijable = new ArrayList<>();
		String operacija = null;
		try {
			while ((s = reader.readLine()) != null) {
				if (s.contains("IDN")) {
					s = s.replaceAll("^\\s+", "");
					arraySplit = s.split(" ");
					s = reader.readLine();
					if (s.contains("PRIDRUZI")) {
						if (!varijable.contains(arraySplit[2])) {
							varijable.add(arraySplit[2]);
//							System.out.println("dodajem" + arraySplit[2]);
						}
						while ((s = reader.readLine()) != null) {
							if (s.contains("BROJ")) {
								s = s.replaceAll("^\\s+", "");
								arraySplit = s.split(" ");
								ispis += "\tMOVE %D " + arraySplit[2] +", R0\n\tPUSH R0\n\n";
//								System.out.println("procitan broj " + arraySplit[2]);
								if (operacija != null) {
									switch (operacija) {
									case "*" :
										ispis += "\tCALL MUL\n\n";
										break;
									case "/" :
										ispis += "\tCALL DIV\n\n";
										break;
									case "+" :
										ispis += "\tPOP R1\n\tPOP R0\n\tADD R0, R1, R2\n\tPUSH R2\n\n";
										break;
									case "-" :
										ispis += "\tPOP R1\n\tPOP R0\n\tSUB R0, R1, R2\n\tPUSH R2\n\n";
										break;
									}
									operacija = null;
								}
							} else if (s.contains("IDN")) {
								s = s.replaceAll("^\\s+", "");
								arraySplit = s.split(" ");
								ispis += "\tLOAD R0, (V" + (varijable.indexOf(arraySplit[2])) + ")\n\tPUSH R0\n\n";
								if (operacija != null) {
									switch (operacija) {
									case "*" :
										ispis += "tCALL MUL\n\n";
										break;
									case "/" :
										ispis += "\tCALL DIV\n\n";
										break;
									case "+" :
										ispis += "\tPOP R1\n\tPOP R0\n\tADD R0, R1, R2\n\tPUSH R2\n\n";
										break;
									case "-" :
										ispis += "\tPOP R1\n\tPOP R0\n\tSUB R0, R1, R2\n\tPUSH R2\n\n";
										break;
									}
									operacija = null;
								}
//								System.out.println(varijable + " sadrze " + arraySplit[2] + " = " + varijable.contains(arraySplit[2]) + " , i to na mjestu " + varijable.indexOf(arraySplit[2]));
//								System.out.println("\tLOAD R0, (V" + (varijable.indexOf(arraySplit[2])) + ")\n\tPUSH R0\n");
								
							} else if (s.contains("PLUS")) {
								operacija = "+";
							} else if (s.contains("MINUS")) {
								operacija = "-";
							} else if (s.contains("PUTA")) {
								operacija = "*";
							} else if (s.contains("DIJELI")) {
								operacija = "/";
							} else if (s.contains("<lista_naredbi>")) {
								ispis += "\tPOP R0\n\tSTORE R0, (V" + (varijable.size()-1) + ")\n\n";
								break;
							}
						}
					}
				} else if (s.contains("KR_ZA")) {
					
				}
				
//				if (s.contains("IDN")) {
//					s = s.replaceAll("^\\s+", "");
//					arraySplit = s.split(" ");
//					s = reader.readLine();
//					if (s.contains("PRIDRUZI")) {
//						Collections.reverse(jedinke);
//						boolean postoji = false;
//						for (Jedinka jedinka : jedinke) {
//							
//							if (jedinka.nizZnakova.equals(arraySplit[2])) {
//								if (jedinka.definiraniBlok > blok) {
//									jedinke.add(new Jedinka(UZnak.valueOf(arraySplit[0]), Integer.parseInt(arraySplit[1]), arraySplit[2], blok));
//									postoji = true;
//									
//								}
//							}
//						}
//						if (!postoji) {
//							jedinke.add(new Jedinka(UZnak.valueOf(arraySplit[0]), Integer.parseInt(arraySplit[1]), arraySplit[2], blok));
//						}
//						Collections.reverse(jedinke);
//
//					} else if (s.contains("KR_OD")) {
//						blok++;
//						jedinke.add(new Jedinka(UZnak.valueOf(arraySplit[0]), Integer.parseInt(arraySplit[1]), arraySplit[2], blok));
//					} else {
////						System.out.println(jedinke);
//						Collections.reverse(jedinke);
//						boolean postoji = false;
//						for (Jedinka jedinka : jedinke) {
//							postoji = false;
//							if (jedinka.nizZnakova.equals(arraySplit[2])) {	
//								if (jedinka.red < Integer.parseInt(arraySplit[1])) {
////									ispis += arraySplit[1] + " " + jedinka.red + " " + arraySplit[2] + "\n";
//									postoji = true;
//								}
//								break;
//							} 
//						}
//						Collections.reverse(jedinke);
//						if (!postoji) {
//							ispis += "err " + arraySplit[1] + " " + arraySplit[2] + "\n";
//							break;
//						}
//					}
//				} else if (s.contains("KR_AZ")) {
//					for (Jedinka jedinka : jedinke) {
//						if (jedinka.definiraniBlok == blok) {
//							zaBrisati.add(jedinka);
//						}
//					}
//					jedinke.removeAll(zaBrisati);
//					zaBrisati.clear();
//					blok--;
//				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
		ispis = ispis.substring(0, ispis.length()-1);
		ispis += "\tLOAD R6, (V" + varijable.indexOf("rez") + ")\n\tHALT\n";
		int i = 0;
		for (String v : varijable) {
			ispis += "V" + (i++) + "\tDW 0 ; " + v + "\n";
		}
		ispis +="\nMD_SGN\tMOVE 0, R6\n\tXOR R0, 0, R0\n\tJP_P MD_TST1\n\tXOR R0, -1, R0\n\tADD R0, 1, R0\n\tMOVE 1, R6\nMD_TST1\tXOR R1, 0, R1\n\tJP_P MD_SGNR\n\tXOR R1, -1, R1" +
				"\n\tADD R1, 1, R1\n\tXOR R6, 1, R6\nMD_SGNR\tRET\n\nMD_INIT POP R4 ; MD_INIT ret addr\n\tPOP R3 ; M/D ret addr\n\tPOP R1 ; op2\n\tPOP R0 ; op1\n\tCALL MD_SGN" +
				"\n\tMOVE 0, R2 ; init rezultata\n\tPUSH R4 ; MD_INIT ret addr\n\tRET\n\nMD_RET\tXOR R6, 0, R6 ; predznak?\n\tJP_Z MD_RET1\n\tXOR R2, -1, R2 ; promijeni predznak" + 
				"\n\tADD R2, 1, R2\nMD_RET1\tPOP R4 ; MD_RET ret addr\n\tPUSH R2 ; rezultat\n\tPUSH R3 ; M/D ret addr\n\tPUSH R4 ; MD_RET ret addr\n\tRET\n\nMUL\tCALL MD_INIT" +
				"\n\tXOR R1, 0, R1\n\tJP_Z MUL_RET ; op2 == 0\n\tSUB R1, 1, R1\nMUL_1\tADD R2, R0, R2\n\tSUB R1, 1, R1\n\tJP_NN MUL_1 ; >= 0?\nMUL_RET\tCALL MD_RET\n\tRET" +
				"\n\nDIV\tCALL MD_INIT\n\tXOR R1, 0, R1\n\tJP_Z DIV_RET ; op2 == 0\nDIV_1\tADD R2, 1, R2\n\tSUB R0, R1, R0\n\tJP_NN DIV_1\n\tSUB R2, 1, R2\nDIV_RET\tCALL MD_RET\n\tRET";

		System.out.println(ispis);
//		System.out.println(varijable);
		zapisi();
//		System.out.println(jedinke);
	}
	private static void zapisi() throws UnsupportedEncodingException, FileNotFoundException, IOException {
		Writer writer = null;
		try{
			writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream("a.frisc"), "utf-8"));
			writer.write(ispis);
//			System.out.println("ispis-" + ispis);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {writer.close();} catch (Exception ex) {}
		}
	}
}