package lab1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


enum UniformniZnak {
	IDN, BROJ, OP_PRIDRUZI,	OP_PLUS, OP_MINUS, OP_PUTA, OP_DIJELI, L_ZAGRADA, D_ZAGRADA, KR_ZA, KR_OD, KR_DO, KR_AZ;
}


class Jedinka1 {
	UniformniZnak znak;
	int red;
	String nizZnakova;
	
	public Jedinka1(UniformniZnak znak, int red, String nizZnakova) {
		this.znak = znak;
		this.red = red;
		this.nizZnakova = nizZnakova;
	}
	
	public String toString() {
		return znak + " " + red + " " + nizZnakova;
	}
}

public class LeksickiAnalizator {
	
	
	static int brRed;
	static ArrayList<Jedinka1> jedinke = new ArrayList<>();
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		brRed = 0;
		
		while((s = reader.readLine()) != null) {
			brRed++;
			provjeri(s);
		}
		for (Jedinka1 j : jedinke) {
			System.out.println(j);
		}
	}

	private static void provjeri(String string) {
		char[] znakovi;
		znakovi = string.toCharArray();
		String s = "";
		for (int i = 0; i < znakovi.length; i++) {
			if (znakovi[i] == ' ' || znakovi[i] == '\n') continue;
			if (znakovi[i] >= 'a' && znakovi[i] <= 'z') {
				boolean kljucnaRijec = false;
				if (znakovi[i] == 'z') {
					if (i+1 != znakovi.length) {
						if (znakovi[i+1] == 'a' && znakovi[i+2] == ' ') {
							jedinke.add(new Jedinka1(UniformniZnak.KR_ZA, brRed, "za"));
							s = "";
							i++;
							kljucnaRijec = true;
							continue;
						}
					}
				}
				if (znakovi[i] == 'a') {
					if (i+1 != znakovi.length && i+1 != znakovi.length) {
						if (znakovi[i+1] == 'z' && znakovi.length == i+2) {
							jedinke.add(new Jedinka1(UniformniZnak.KR_AZ, brRed, "az"));
							s = "";
							i++;
							kljucnaRijec = true;
							continue;
						}
					}
				}
				if (znakovi[i] == 'o') {
					if (i+2 != znakovi.length && i+1 != znakovi.length) {
						if (znakovi[i+1] == 'd' && znakovi[i+2] == ' ') {
							jedinke.add(new Jedinka1(UniformniZnak.KR_OD, brRed, "od"));
							s = "";
							i++;
							kljucnaRijec = true;
							continue;
						}
					}
				}
				if (znakovi[i] == 'd') {
					if (i+1 != znakovi.length && i+1 != znakovi.length) {
						if (znakovi[i+1] == 'o' && znakovi[i+2] == ' ') {
							jedinke.add(new Jedinka1(UniformniZnak.KR_DO, brRed, "do"));
							s = "";
							i++;
							kljucnaRijec = true;
							continue;
						}
					}
				}
				
				if (!kljucnaRijec) {
					while (i < znakovi.length) {
						if ((znakovi[i] >= 'a' && znakovi[i] <= 'z') || (znakovi[i] >= '0' && znakovi[i] <= '9')) {
							s += znakovi[i++];
							if (i == znakovi.length) {
								jedinke.add(new Jedinka1(UniformniZnak.IDN, brRed, s));
								s = "";
								break;
							}
						} else {
							jedinke.add(new Jedinka1(UniformniZnak.IDN, brRed, s));
							s = "";
							i--;
							break;
						}
					}
				}
			} else if (znakovi[i] >= '0' && znakovi[i] <= '9') {
				while (i < znakovi.length) {
					if (znakovi[i] >= '0' && znakovi[i] <= '9') {
						s += znakovi[i++];
						if (i == znakovi.length) {
							jedinke.add(new Jedinka1(UniformniZnak.BROJ, brRed, s));
							s = "";
							break;
						}
					} else {
						jedinke.add(new Jedinka1(UniformniZnak.BROJ, brRed, s));
						s = "";
						i--;
						break;
					}
				}
				
			} else {
				s += znakovi[i];
				switch (s) {
				case ("+") :
					jedinke.add(new Jedinka1(UniformniZnak.OP_PLUS, brRed, s));
					s = "";
					break;
				case ("-") :
					jedinke.add(new Jedinka1(UniformniZnak.OP_MINUS, brRed, s));
					s = "";
					break;
				case ("=") :
					jedinke.add(new Jedinka1(UniformniZnak.OP_PRIDRUZI, brRed, s));
					s = "";
					break;
				case ("/") :
					if (i+1 != znakovi.length) {
						if (znakovi[i+1] == '/') return;
					}
					jedinke.add(new Jedinka1(UniformniZnak.OP_DIJELI, brRed, s));
					s = "";
					break;
				case ("*") :
					jedinke.add(new Jedinka1(UniformniZnak.OP_PUTA, brRed, s));
					s = "";
					break;
				case ("(") :
					jedinke.add(new Jedinka1(UniformniZnak.L_ZAGRADA, brRed, s));
					s = "";
					break;
				case (")") :
					jedinke.add(new Jedinka1(UniformniZnak.D_ZAGRADA, brRed, s));
					s = "";
					break;
				default : 
					s = "";
					break;
				}
			}
		}
	}
}
