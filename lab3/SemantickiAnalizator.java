package lab3;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;


enum UZnak {
	IDN, BROJ, OP_PRIDRUZI,	OP_PLUS, OP_MINUS, OP_PUTA, OP_DIJELI, L_ZAGRADA, D_ZAGRADA, KR_ZA, KR_OD, KR_DO, KR_AZ;
}

class Jedinka2 {
	UZnak znak;
	int red;
	String nizZnakova;
	int definiraniBlok;
	
	public Jedinka2(UZnak znak, int red, String nizZnakova, int definiraniBlok) {
		this.znak = znak;
		this.red = red;
		this.nizZnakova = nizZnakova;
		this.definiraniBlok = definiraniBlok;
	}
	
	public String toString() {
		return znak + " " + red + " " + nizZnakova + " " + definiraniBlok;
	}
}

public class SemantickiAnalizator {
	
	//blok je dubina
	static int blok = 0;

	static String ispis = "";
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		String[] arraySplit;
		ArrayList<Jedinka2> jedinke = new ArrayList<>();
		Queue<Jedinka2> zaBrisati = new LinkedList<>();
		try {
			while ((s = reader.readLine()) != null) {
				
				if (s.contains("IDN")) {
					s = s.replaceAll("^\\s+", "");
					arraySplit = s.split(" ");
					s = reader.readLine();
					if (s.contains("PRIDRUZI")) {
						Collections.reverse(jedinke);
						boolean postoji = false;
						for (Jedinka2 jedinka : jedinke) {
							
							if (jedinka.nizZnakova.equals(arraySplit[2])) {
								if (jedinka.definiraniBlok > blok) {
									jedinke.add(new Jedinka2(UZnak.valueOf(arraySplit[0]), Integer.parseInt(arraySplit[1]), arraySplit[2], blok));
									postoji = true;
								}
							}
						}
						if (!postoji) {
							jedinke.add(new Jedinka2(UZnak.valueOf(arraySplit[0]), Integer.parseInt(arraySplit[1]), arraySplit[2], blok));
						}
						Collections.reverse(jedinke);

					} else if (s.contains("KR_OD")) {
						blok++;
						jedinke.add(new Jedinka2(UZnak.valueOf(arraySplit[0]), Integer.parseInt(arraySplit[1]), arraySplit[2], blok));
					} else {
//						System.out.println(jedinke);
						Collections.reverse(jedinke);
						boolean postoji = false;
						for (Jedinka2 jedinka : jedinke) {
							postoji = false;
							if (jedinka.nizZnakova.equals(arraySplit[2])) {	
								if (jedinka.red < Integer.parseInt(arraySplit[1])) {
									ispis += arraySplit[1] + " " + jedinka.red + " " + arraySplit[2] + "\n";
									postoji = true;
								}
								break;
							} 
						}
						Collections.reverse(jedinke);
						if (!postoji) {
							ispis += "err " + arraySplit[1] + " " + arraySplit[2] + "\n";
							break;
						}
					}
				} else if (s.contains("KR_AZ")) {
					for (Jedinka2 jedinka : jedinke) {
						if (jedinka.definiraniBlok == blok) {
							zaBrisati.add(jedinka);
						}
					}
					jedinke.removeAll(zaBrisati);
					zaBrisati.clear();
					blok--;
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
		}
		ispis = ispis.substring(0, ispis.length()-1);
		System.out.println(ispis);
//		System.out.println(jedinke);
	}
}