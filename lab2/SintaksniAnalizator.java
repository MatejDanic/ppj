package lab2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

enum UniZnak {
	IDN, BROJ, OP_PRIDRUZI,	OP_PLUS, OP_MINUS, OP_PUTA, OP_DIJELI, L_ZAGRADA, D_ZAGRADA, KR_ZA, KR_OD, KR_DO, KR_AZ;
}

class Jedinka {
	UniZnak znak;
	int red;
	String niz;
	
	public Jedinka(UniZnak znak, int red, String niz) {
		this.znak = znak;
		this.red = red;
		this.niz = niz;
	}
	
	public String toString() {
		return znak + " " + red + " " + niz;
	}
}
public class SintaksniAnalizator {
	
	static ArrayList<Jedinka> jedinke = new ArrayList<>();
	static int index = 0;
	static String ispis;
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s;
		Jedinka jedinka;
		while((s = reader.readLine()) != null && s != "") {
			String[] dijelovi = s.split(" ");
			try {
				if (s != "") {
					jedinka = new Jedinka(UniZnak.valueOf(dijelovi[0].toUpperCase()), Integer.parseInt(dijelovi[1]), dijelovi[2]);
					jedinke.add(jedinka);
				}
			} catch (IllegalArgumentException e) {
//				e.printStackTrace();
			}
		}
//		jedinke.forEach((j) -> System.out.println(j));
		parsiraj(index, "lista_naredbi", 0);
		if (ispis != null && !ispis.startsWith("err")) System.out.println(ispis);
//		System.out.println("aaa");
	}

	private static void parsiraj(int i, String cvor, int dubina) {
//		System.out.println("a");
		if (ispis != null && ispis.startsWith("err")) return;
		if (dubina == 0) {
			ispis = "<program>\n";
			dubina = 1;
		}
		switch (cvor) {
		
		case "lista_naredbi" :
			dodajDubinu(dubina);
			ispis += "<lista_naredbi>\n";
			if (index < jedinke.size()) {
				if (jedinke.get(index).znak == UniZnak.KR_ZA || jedinke.get(index).znak == UniZnak.IDN) {
					parsiraj(index, "naredba", dubina+1);
					parsiraj(index, "lista_naredbi", ++dubina);
				} else if (jedinke.get(index).znak == UniZnak.KR_AZ) {
					dodajDubinu(++dubina);
					ispis += "$\n";
				} else {
					ispisiGresku();
					return;
				}
			} else {
				dodajDubinu(++dubina);
				ispis += "$\n";
				return;
			}
			break;
			
		case "naredba" :
			dodajDubinu(dubina);
			ispis += "<naredba>\n";
			if (jedinke.get(index).znak == UniZnak.IDN) {
				parsiraj(index, "naredba_pridruzivanja", dubina+1);
			} else if (jedinke.get(index).znak == UniZnak.KR_ZA) {
				parsiraj(index, "za_petlja", dubina+1);
			} else {
				ispisiGresku();
				return;
			}
			break;
			
		case "naredba_pridruzivanja" : 
			dodajDubinu(dubina);
			ispis += "<naredba_pridruzivanja>\n";
			if (jedinke.get(index).znak == UniZnak.IDN) {
				dodajDubinu(++dubina);
				ispis += jedinke.get(index++) + "\n";
				if (jedinke.get(index).znak != UniZnak.OP_PRIDRUZI) {
					ispisiGresku();
					return;
				}
			}
			dodajDubinu(dubina);
			ispis += jedinke.get(index++) + "\n";
			parsiraj(index, "e", dubina);
			break;
		
		case "za_petlja" :
			dodajDubinu(dubina);
			ispis += "<za_petlja>\n";
			if (jedinke.get(index).znak == UniZnak.KR_ZA) {
				dodajDubinu(++dubina);
				ispis += jedinke.get(index++) + "\n";
				if (index < jedinke.size()) {
					if (jedinke.get(index).znak != UniZnak.IDN) {
						ispisiGresku();
						return;
					}
				} else {
					ispisiGresku();
					return;
				}
				dodajDubinu(dubina);
				ispis += jedinke.get(index++) + "\n";
				if (index < jedinke.size()) {
					if (jedinke.get(index).znak != UniZnak.KR_OD) {
						ispisiGresku();
						return;
					}
				} else {
					ispisiGresku();
					return;
				}
				dodajDubinu(dubina);
				ispis += jedinke.get(index++) + "\n";
				parsiraj(index, "e", dubina);
				if (index < jedinke.size()) {
					if (jedinke.get(index).znak != UniZnak.KR_DO) {
						ispisiGresku();
						return;
					}
				} else {
					ispisiGresku();
					return;
				}
				dodajDubinu(dubina);
				ispis += jedinke.get(index++) + "\n";
				parsiraj(index, "e", dubina);
				parsiraj(index, "lista_naredbi", dubina);
			
				if (index < jedinke.size()) {
					if (jedinke.get(index).znak != UniZnak.KR_AZ) {
						ispisiGresku();
						return;
					}
				dodajDubinu(dubina);
				ispis += jedinke.get(index++) + "\n";
				}
			}
			break;
			
		case "e" :
			dodajDubinu(dubina);
			ispis += "<E>\n";
			if (index < jedinke.size()) {
				if (jedinke.get(index).znak == UniZnak.IDN 
						|| jedinke.get(index).znak == UniZnak.BROJ 
						|| jedinke.get(index).znak == UniZnak.OP_PLUS 
						|| jedinke.get(index).znak == UniZnak.OP_MINUS 
						|| jedinke.get(index).znak == UniZnak.L_ZAGRADA) {
					parsiraj(index, "t", dubina+1);
					parsiraj(index, "e_lista", dubina+1);
				}
			} else {
				ispisiGresku();
				return;
			}
			break;
			
		case "e_lista" :
			dodajDubinu(dubina);
			ispis += "<E_lista>\n";
			dodajDubinu(++dubina);
			if (index < jedinke.size()) {
				if (jedinke.get(index).znak == UniZnak.OP_PLUS || jedinke.get(index).znak == UniZnak.OP_MINUS) {
					ispis += jedinke.get(index++) + "\n";
					parsiraj(index, "e", dubina);
				} else {
					ispis += "$\n";
				}
			} else {
				ispis += "$\n";
			}
			break;
			
		case "t" : 
			dodajDubinu(dubina);
			ispis += "<T>\n";
			if (index < jedinke.size()) {
				if (jedinke.get(index).znak == UniZnak.IDN 
						|| jedinke.get(index).znak == UniZnak.BROJ 
						|| jedinke.get(index).znak == UniZnak.OP_PLUS 
						|| jedinke.get(index).znak == UniZnak.OP_MINUS 
						|| jedinke.get(index).znak == UniZnak.L_ZAGRADA) {
					parsiraj(index, "p", dubina+1);
					parsiraj(index, "t_lista", dubina+1);
				}
			} else {
				ispisiGresku();
				return;
			}
			break;
			
		case "t_lista" :
			dodajDubinu(dubina);
			ispis += "<T_lista>\n";
			dodajDubinu(++dubina);
			if (index < jedinke.size()) {
				if (jedinke.get(index).znak == UniZnak.OP_PUTA 
						|| jedinke.get(index).znak == UniZnak.OP_DIJELI) {
					ispis += jedinke.get(index++) + "\n";
					parsiraj(index, "t", dubina);
				} else {
				ispis += "$\n";
				} 
			} else {
				ispis += "$\n";
			}
			break;
			
		case "p" : 
			dodajDubinu(dubina);
			ispis += "<P>\n";
			dodajDubinu(++dubina);
			if (index < jedinke.size()) {
				if (jedinke.get(index).znak == UniZnak.OP_PLUS || jedinke.get(index).znak == UniZnak.OP_MINUS) {
					ispis += jedinke.get(index++) + "\n";
					parsiraj(index, "p", dubina);
				} else if (jedinke.get(index).znak == UniZnak.L_ZAGRADA) {
					ispis += jedinke.get(index++) + "\n";
					parsiraj(index, "e", dubina);
					if (jedinke.get(index).znak != UniZnak.D_ZAGRADA) {
						ispisiGresku();
						return;
					}
					dodajDubinu(dubina);
					ispis += jedinke.get(index++) + "\n";
				} else if (jedinke.get(index).znak == UniZnak.IDN) {
					ispis += jedinke.get(index++) + "\n";
				} else if (jedinke.get(index).znak == UniZnak.BROJ) {
					ispis += jedinke.get(index++) + "\n";
				}
			} else {
				ispisiGresku();
				return;
			}
			break;
		}
	}

	private static void ispisiGresku() {
		ispis = "err ";
		if (index < jedinke.size()) ispis += jedinke.get(index);
		else ispis += "kraj";
		System.out.println(ispis);
	}

	private static void dodajDubinu(int dubina) {
		for (int i = 0; i < dubina; i++) {
			ispis += " ";
		}
	}
}
